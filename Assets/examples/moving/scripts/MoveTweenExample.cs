﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTweenExample : MonoBehaviour
{
    [Header("Tween parameters")]
    [Space()]

    [Tooltip("Transform which we are moving.")]
    public Transform whichTransform;

    [Tooltip("Transform target to where our GameObject will move")]
    public Transform target;

    [Tooltip("Delay for tween in seconds.")]
    public float delay = 0.0f;

    [Tooltip("Duration of the tween in seconds.")]
    public float duration = 0.3f;

    [Tooltip("Ease type based on the AnimationCurve")]
    public AnimationCurve ease = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);

    Coroutine coroutine;

    Vector3 defaultPosition;

    // Start is called before the first frame update
    void Start()
    {
        defaultPosition = whichTransform.position;
    }

    public void Move()
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        coroutine = StartCoroutine(TweenLibrary.TweenTransformPosition(whichTransform, whichTransform.position, target.position, duration, delay, ease));
    }

    public void Restart()
    {
        whichTransform.position = defaultPosition;
    }
}
