﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTweenExampleSimple : MonoBehaviour
{
    [Header("Tween parameters")]
    public Transform target;
    public float duration = 0.3f;
    public AnimationCurve ease;

    // Private variables
    private float timePassedMoving = 0f;
    private Vector3 distance;
    private bool isMoving;
    private Vector3 defaultPosition;

    void Start()
    {
        defaultPosition = transform.position;
    }

    void Update()
    {
        if (!isMoving)
        {
            return;
        }

        // Get a 0-1 value to use against an ease curve
        float t = timePassedMoving / duration;

        t = ease.Evaluate(t);

        transform.position = defaultPosition + distance * t;

        // Add frame time to timer and check if we're passed duration
        timePassedMoving += Time.deltaTime;
        if (timePassedMoving > duration)
        {
            // Stop moving and make sure we're exactly at the target
            isMoving = false;
            transform.position = target.position;
        }
    }

    public void Move()
    {
        isMoving = true;
        // Calculate distance before we start moving
        distance = target.position - defaultPosition;
        // and restart the timer
        timePassedMoving = 0f;
    }

    public void Restart()
    {
        transform.position = defaultPosition;
    }

}
