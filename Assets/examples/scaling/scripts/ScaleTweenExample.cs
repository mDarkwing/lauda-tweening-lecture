﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTweenExample : MonoBehaviour
{
    [Header("Tween parameters")]
    [Space()]

    [Tooltip("Transform which we are scaling.")]
    public Transform whichTransform;

    [Tooltip("Size to which our object will be rescaled")]
    public Vector3 to;

    [Tooltip("Delay for tween in seconds.")]
    public float delay = 0.0f;

    [Tooltip("Duration of the tween in seconds.")]
    public float duration = 0.3f;

    [Tooltip("Ease type based on the AnimationCurve")]
    public AnimationCurve ease = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);

    Coroutine coroutine;

    Vector3 defaultScale;

    // Start is called before the first frame update
    void Start()
    {
        defaultScale = whichTransform.localScale;
    }

    public void Scale()
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        coroutine = StartCoroutine(TweenLibrary.TweenTransformLocalScale(whichTransform, whichTransform.localScale, to, duration, delay, ease));
    }

    public void Restart()
    {
        whichTransform.localScale = defaultScale;
    }
}
