﻿using System.Collections;
using UnityEngine;

//We are wrapping the tween functions inside a static class so that we can have "global" access to them.
public static class TweenLibrary
{
    /// <summary>
    /// Coroutine that moves objects position from "from" to "to" positions. You can specify the delay and duration as well
    /// as ease type using AnimationCurve type. You have to use StartCoroutine() from any MonoBehaviour to run this tween.
    /// </summary>
    /// <param name="whichTransform">Transform that is target of the tween.</param>
    /// <param name="from">Vector3 position from which the tween starts.</param>
    /// <param name="to">Vector3 position which is the destination of the tween.</param>
    /// <param name="duration">Duration of the tween in seconds.</param>
    /// <param name="delay">Delay of the tween in seconds.</param>
    /// <param name="ease">Provides ease type for the tween if supplied.</param>
    /// <returns>Returns IEnumerator so that this method can be started as a Coroutine.</returns>
    public static IEnumerator TweenTransformPosition(Transform whichTransform, Vector3 from, Vector3 to, float duration = 0.3f, float delay = 0.0f, AnimationCurve ease = null)
    {
        //If there is a delay we wait for that amount of time otherwise run the tween immediatly.
        if (delay > 0.0f)
        {
            yield return new WaitForSeconds(delay);
        }

        //Get the distance between the final and starting locations.
        Vector3 dist = to - from;

        //Repeat this code every frame using a counter that counts until the duration is over.
        //Counter starts at 0.0f and adds Time.delta time each iteration (which can be considered as duration of 1 frame in our game)
        //Finally we calculate the 't' parameter for linear interpolation equation which tells us at what percentage of
        //the tween duration we are in normalized form, aka t E {0.0f, 1.0f}
        for (float counter = 0.0f, t = 0.0f; counter <= duration; counter += Time.deltaTime, t = counter / duration)
        {
            //NOTE: You can test different versions of the easing namely between AnimationCurve and function 
            //by commenting/uncommenting code inside the respective regions.

            #region Ease using AnimationCurve type
            //We "ease" the t value of the linear interpolation equation if there is an AnimationCurve type object in args
            if (ease != null)
            {
                t = ease.Evaluate(t);
            }
            //We use the linear interpolation equation given to move the object.
            whichTransform.position = from + dist * t;
            #endregion

            #region Ease using functions (quadratic, cubic, etc...)
            // //Example of cubic function y = x^2. The quadratic magic happens in the t*t part which results in our
            // //tween behaving like a graph of said function in this case right half of the "parabola"
            // whichTransform.position = from + dist * t * t;

            // // //Similiar to the example above but uses cubic function where y = x^3
            // // whichTransform.position = from + dist * t * t * t;
            #endregion

            //We pause the execution of the loop for 1 frame to simulate realtime movement
            yield return null;
        }

        //Since we are using the imprecise version of the equation for simplicity we have to "snap" our transform to
        //the final location. Imprecision happens due the floating point arithmetics in the counter <= duration since it's
        //not guaranteed that sum of the frame durations will be equal to the duration of the tween which in turn makes our t
        //not be 1.0 most of the time.
        whichTransform.position = to;
    }


    /// <summary>
    /// Coroutine that moves objects local position from "from" to "to" positions. You can specify the delay and duration as well
    /// as ease type using AnimationCurve type. You have to use StartCoroutine() from any MonoBehaviour to run this tween.
    /// </summary>
    /// <param name="whichTransform">Transform that is target of the tween.</param>
    /// <param name="from">Vector3 local position from which the tween starts.</param>
    /// <param name="to">Vector3 local position which is the destination of the tween.</param>
    /// <param name="duration">Duration of the tween in seconds.</param>
    /// <param name="delay">Delay of the tween in seconds.</param>
    /// <param name="ease">Provides ease type for the tween if supplied.</param>
    /// <returns>Returns IEnumerator so that this method can be started as a Coroutine.</returns>
    public static IEnumerator TweenTransformLocalPosition(Transform whichTransform, Vector3 from, Vector3 to, float duration = 0.3f, float delay = 0.0f, AnimationCurve ease = null)
    {
        if (delay > 0.0f)
        {
            yield return new WaitForSeconds(delay);
        }

        Vector3 dist = to - from;

        for (float counter = 0.0f, t = 0.0f; counter <= duration; counter += Time.deltaTime, t = counter / duration)
        {
            if (ease != null)
            {
                t = ease.Evaluate(t);
            }
            whichTransform.localPosition = from + dist * t;
            yield return null;
        }
        whichTransform.localPosition = to;
    }

    /// <summary>
    /// Coroutine that rescales objects local scale from "from" to "to". You can specify the delay and duration as well
    /// as ease type using AnimationCurve type. You have to use StartCoroutine() from any MonoBehaviour to run this tween.
    /// </summary>
    /// <param name="whichTransform">Transform that is target of the tween.</param>
    /// <param name="from">Vector3 local scale from which the tween starts.</param>
    /// <param name="to">Vector3 local scale which is the destination of the tween.</param>
    /// <param name="duration">Duration of the tween in seconds.</param>
    /// <param name="delay">Delay of the tween in seconds.</param>
    /// <param name="ease">Provides ease type for the tween if supplied.</param>
    /// <returns>Returns IEnumerator so that this method can be started as a Coroutine.</returns>
    public static IEnumerator TweenTransformLocalScale(Transform whichTransform, Vector3 from, Vector3 to, float duration = 0.3f, float delay = 0.0f, AnimationCurve ease = null)
    {
        if (delay > 0.0f)
        {
            yield return new WaitForSeconds(delay);
        }

        Vector3 dist = to - from;

        for (float counter = 0.0f, t = 0.0f; counter <= duration; counter += Time.deltaTime, t = counter / duration)
        {
            if (ease != null)
            {
                t = ease.Evaluate(t);
            }
            whichTransform.localScale = from + dist * t;
            yield return null;
        }
        whichTransform.localScale = to;
    }
}
