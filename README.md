![alt text](https://scontent.fbeg4-1.fna.fbcdn.net/v/t1.0-9/149522248_216539416844771_4198770033382807135_o.jpg?_nc_cat=110&ccb=3&_nc_sid=b386c4&_nc_ohc=QM9ZVuD9OdUAX8fJUBY&_nc_ht=scontent.fbeg4-1.fna&oh=777925eb1d2902532e755bf898099897&oe=6064421E)

# Description

This readme file provides a brief summary of topics we passed on the lecture. It is consisted of three sections (Math Stuff, Tweening, Implementation in Unity) which are further divided into subsections for their relevant topic. You can find many links when reading this that go into further details depending on the current topic. Lecture was held on a [Lauda](https://www.facebook.com/lauda.edu/) [discord server](https://discord.gg/NB68JnaA) on [February 22. 2021](https://www.facebook.com/events/431651344766558/) in a form of a live workshop.

# Math Stuff

### Normalization
To normalize a value inside a given range we use the formula:

> range {x0,x1}

> d = x1 - x0

> x = val / d => x E (0,1)



Where 'val' is the amount that we want to normalize and 'd' is the magnitude of the range (say the range is {0,d}). The x is then normalized value which means it's between 0 and 1 which coincidentaly fits the concept of percentage as in how much percentage does val take in d.

Example:

> val = 1

> d = 3

> x = 1/3 => val is 33% of d

> val =2

> d = 3

> x = 2/3 => val is 66% of d

Given this if we steadily increase the val we can traverse d from 0 to 100% aka 0 to 1. In programming percentage is usualy replaced with normalized values in range {0,1}. Note that we can have val beyond the given range which will also give us the percentage of how much val is contained in d as in following example:

> val = 5

> d = 3

> x = 5/3 => val is 166% of d

Normalization can be also used when talking about vectors, where [normalized vector](https://en.wikipedia.org/wiki/Unit_vector) represents a vector that loses it's information about magnitude (it's magnitude is always 1) but retains information about direction.

### Linear Interpolation

[Linear interpolation](https://en.wikipedia.org/wiki/Linear_interpolation) is a method of approximating points between two values using linear function. For this we use the formula:

> x = x0 + t * (x1-x0)

Where:

> x - is our point at a given moment

> x0 - starting point

> x1 - ending point

> t - value between {0,1} that denotes at what percentage of the way between x0 and x1 we are. Note that t can be lesser or greater than borders of the range which means that we are over or behind the given range (say t = 1.5 which means that we are 50% behind the x1).

### Curves

When we say curves we usually mean of [Bezier](https://en.wikipedia.org/wiki/B%C3%A9zier_curve) curves in [Unity](https://unity.com/). Bezier curve is a type of curve that is described by control points. As most things in maths this is neatly tied to the linear interpolation as in that lerp is a special case of bezier curve of first degree. Higher order/degree bezier curves are constructed by injecting formula for linear interpolation inside previous order bezier.

First order/degree ~ lerp is described by two control points p0 and p1

> p = p0 + t * (p1-p0)

![Alt Text](https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/B%C3%A9zier_1_big.gif/240px-B%C3%A9zier_1_big.gif)

Second order/degree is described by three control points p0,p1,p2. We construct the curve by building on the previous order and inject two seprate first order curves (lerps) namely between p0,p1 and p1,p2 and fit them in the first order (lerp) equation

> a = p0 + t * (p1-p0)

> b = p1 + t * (p2-p1)

> p = a + t * b

> p = (p0 + t * (p1-p0)) + t * (p1 + t * (p2-p1))

![Alt Text](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/B%C3%A9zier_2_big.gif/240px-B%C3%A9zier_2_big.gif)

> Note: Most of these concepts translate to higher dimensions as in vector spaces. These concepts fully work in 1D, 2D and 3D spaces which we will build upon later (important in relation to Unity given that we have 2D/3D spaces with [Vector2](https://docs.unity3d.com/ScriptReference/Vector2.html) and [Vector3](https://docs.unity3d.com/ScriptReference/Vector3.html) types)

# Tweening

[Tweening](https://en.wikipedia.org/wiki/Inbetweening) is a term used to describe a simple animation in field of computer graphics. The application of tweening is spanned across multiple fields like game development, video editing, animation etc. Tweening can enhance user experience as as such can be considered an important part of your UX development whether thats in case of UI (most common usage) or other parts of your game. For purposes of this lecture we will consider a tween as a minimal unit of animation as in animating one value whether that is position, scale, rotation, alpha, color etc... When talking about tweening we can encounter the term easing which refers to the process of smoothing out an animation using a function or a curve.

Good material for starting point of this topic would be [Robert Penners](http://robertpenner.com/easing/) easing equations. Here are some good links to read about this:
 - [Easing Functions](https://easings.net/)
 - [Dynamic Visuals Chapter](http://robertpenner.com/easing/penner_chapter7_tweening.pdf)
 - [Interpolation Tricks](http://sol.gfxile.net/interpolation/)

# Implementation in Unity

So how do all the topics above tie into our Unity implementation? For this we will use position tween from the project and break it down line by line using the topics discussed above as well as explain newly introduced concepts. The bulk of the code for position tween (aka moving object from one point to another):

This is the declaration of our tween method:

```CSharp
public static IEnumerator TweenTransformPosition(Transform whichTransform, Vector3 from, Vector3 to, float duration = 0.3f, float delay = 0.0f, AnimationCurve ease = null)
    {
        if (delay > 0.0f)
        {
            yield return new WaitForSeconds(delay);
        }

        Vector3 dist = to - from;

        for (float counter = 0.0f, t = 0.0f; counter <= duration; counter += Time.deltaTime, t = counter / duration)
        {
            if (ease != null)
            {
                t = ease.Evaluate(t);
            }
            whichTransform.position = from + dist * t;

            // whichTransform.position = from + dist * t * t;
            // whichTransform.position = from + dist * t * t * t;

            yield return null;
        }
        whichTransform.position = to;
    }
```

Lets break down this code line by line:

```CSharp
public static IEnumerator TweenTransformPosition(Transform whichTransform, Vector3 from, Vector3 to, float duration = 0.3f, float delay = 0.0f, AnimationCurve ease = null)
```
 - `public` - means that we make our method [visibile to outside](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/public)
 - `static` - means our method is "global" in a sense that [it is bound to the type](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/static) rather then an instance of an object.
 - `IEnumerator` - [Coroutines](https://docs.unity3d.com/Manual/Coroutines.html) are built on the [IEnumerator](https://docs.microsoft.com/en-us/dotnet/api/system.collections.ienumerator?view=net-5.0) so we have to return this type.
 - `TweenTransformPosition` - name [identifier](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/inside-a-program/identifier-names) for our method.
 - `Transform whichTransform` - parameter of [type](https://docs.microsoft.com/en-us/dotnet/api/system.type?view=net-5.0) [Transform](https://docs.unity3d.com/ScriptReference/Transform.html) and name identifier whichTransform which is used to pass the Transform component of [GameObject](https://docs.unity3d.com/ScriptReference/GameObject.html) we want to animate
 - `Vector3 from` - parameter of type [Vector3](https://docs.unity3d.com/ScriptReference/Vector3.html) and name identifier from which is used to specify starting location in world from where our object will be moved.
 - `Vector3 to` - parameter of type [Vector3](https://docs.unity3d.com/ScriptReference/Vector3.html) and name identifier to which is used to specify destination in world to which our object will move/

Parameters with [default values are optional](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/named-and-optional-arguments) when calling the method and if not specified otherwise they will use the value assigned in the declaration.

 - `float duration = 0.3f` - parameter of type [float](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/floating-point-numeric-types) and name identifier duration which specifies duration of the tween in seconds. 
 - `float delay = 0.0f` // parameter of type float and name identifier duration which specifies duration of the tween in seconds.
 - `AnimationCurve ease = null` // parameter of type [AnimationCurve](https://docs.unity3d.com/ScriptReference/AnimationCurve.html) and name identifier ease which when specified provides a way to ease our animation using the curve.

Here we will check if there is any delay before our tween starts and wait for said amount of time. We also encounter first time the `yield return` code which is bound to the IEnumerator and Coroutines. Our method will pause when it encounters this instruction and wait for the amount specified in the `yield return <value>`. This can be some amount of time, a frame, end of a frame, another coroutine etc... In this case we want to wait for the delay amount of seconds.

```CSharp
        if (delay > 0.0f)
        {
            yield return new WaitForSeconds(delay);
        }
```
 - `if (delay > 0.0f)` - using the [if conditional](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/if-else) we check if there is some duration user wants to wait before executing the tween (in this case delay is bigger than 0)
  - `yield return new WaitForSeconds(delay);` - we pause the execution of a Coroutine by creating a [new WaitForSeconds](https://docs.unity3d.com/ScriptReference/WaitForSeconds.html) object and specify the amount we want to wait in the [constructor](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/constructors).


Next we calculate the distance we need to travel which corresponds to our $(x1 - x0)$ from linear interpolation definition. We cache the result in a variable dist so we don't have to calculate it every time.
```CSharp
    Vector3 dist = to - from;
```

The driving part that makes this tween happen over time is the [for loop](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/for) in conjunction with the yield return null which will wait for one frame. Inside for loop we replace the standard int iterators with a float iterator named `counter`. We set this counter to 0 since this will represent our timer which will count from 0 to `duration` which corresponds to the duration of our tween. We can also use the `counter` and `duration` to form a basis for our $t$ from lerp equation $t * (x1-x0)$ which if you remember was a way for us get a location based on the percentage of the whole distance. That is why we increment `counter` by `Time.deltaTime` which is duration of one frame (and why we wait for one frame inised the loop). Then we simply calculate our `t` as `t=counter/duration`. This also neatly implies that we already have somewhat 'linear' ease that is derived from the lerp equation.

```CSharp
for (float counter = 0.0f, t = 0.0f; counter <= duration; counter += Time.deltaTime, t = counter / duration)
```

The calculation above has a property that will neatly make our default ease type be linear which is derived from the lerp equation. However next line of code ensures that if the user has specified custom easing we apply that.

```CSharp
if (ease != null)
{
    t = ease.Evaluate(t);
}
```

If the following line looks familiar to you it's beacause it is a applied version of our lerp equation which we use to move the object.

```CSharp
whichTransform.position = from + dist * t;
//x = x0 + (x1-x0) * t
//Note that we've swapped the order of operations since multiplication is commutative and our dist is technicaly (x1-x0).
```

The commented lines refer to the alternative method of easing our tween using functions namely quadratic and cubic. This combines methods of Penners easing equation with lerp to create quadratic or cubic ease type. We can see that the only parameter we had to modify was `t`. In similiar fashion if you want to use some other custom function it will probably perform some transformation of `t`.
```CSharp
// whichTransform.position = from + dist * t * t;   //quadratic
// whichTransform.position = from + dist * t * t * t;   //cubic
```

Lastly we instruct our coroutine to wait for one frame using:

```CSharp
yield return null;
```

As a final step since we are using imprecise version of the lerp function we need to ensure our object really ends up on the destination which is exactly what the last line does:
```CSharp
whichTransform.position = to;
```


# Licence

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).
